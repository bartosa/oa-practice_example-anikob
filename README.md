# weather alkalmazás

## Mire való?
A weather alkalmazás megjeleníti adott helyen az időjárás előrejelzést, 5 napra vonatkozóan, 3 órás bontásokban.

## Telepítés

* A program egy docker image-ben indul.
* A docker image elérhető itt: https://hub.docker.com/r/bartosaniko/bartos_practice
* Ehhez terminálba az alábbit kell begépeni:<br/> `docker pull bartosaniko/bartos_practice`
* Figyelem! Az image letöltése néhány percet igénybe vehet.


## weather program indítása és használata

* Indításhoz terminálba az alábbit kell begépeni:<br/>`docker container run -ti bartosaniko/bartos_practice`
* A program ekkor várja a lekérdezendő város nevét és a kapcsolókat, melyek opcionálisak: <br/>
"City and arguments (x- exit): "
* Ekkor be kell gépelni a város nevét, majd entert ütni.

##Alapkimenet
A program alapból megjeleníti a lekérdezett hely nevét és ország kódját, valamint az aktuális hőmérséklet előrejelzést 5 napre előre, 3 órás bontásokban, amennyiben azt megtalálta az adatbázisban.<br/>
Például:<br/>

![Image of default](images/default.png)

##Kapcsolók
A program kimenete kibővíthető néhány kapcsolóval:
* d: description, avagy leírás. Az adott időpont időjárásának leírása. Pl.: Cloud, Rain, Clear...
* f: feel, avagy a hőérzet. Az adott hőmérsékletet számos tényező miatt többnek, vagy kevesebbnek érezhezjük. Például a szél miatt. Ezzel a kapcsolóval megheleníthető a tényleges hőmérséklet mellett a hőérzet is.
* w: warning. Figyelmeztetés jelenik meg eső vagy hó esetén az adott időpontnál.
* m: minimim és maximum. A minimum és maximum hőmérséklet megjelenítése adott időpontban.

Megadásukhoz a helységnév utáni space-t követően van lehetőség, akár ömlesztve, akár külön-külön. A kapcsolóknál a kis-és nagy betű között nincs különbség, ismétlésülre lehetőség adott.
Például:<br/>
![Image of default](images/extended.png)

##Kilépés
A programból való kilépéshez a város neve helyére 'x'-et kell megadni.
	

## API referencia
A szoftver a https://openweathermap.org/ ingyenes, de regisztárcióhoz kötött API-ját használja.

## Szükséges programok
* Docker

## GYIK
* Nem azt a helyet adta vissza, amire gondoltam. Mit tegyek?
  * A helységnév után meg lehet adni az országot. Pl: "Velence" begépelésére az olaszországi várost adja vissza a program. Amennyiben vesszővel elválasztva (szóköz vélkül) hozzáírjuk az ország kódját, már a megfelelő települést kapjuk. Pl: "Velence,hu"
* "Place not found" hibaüzenetet kapok. Ez miért van?
  * Az adatbázis valószínűleg nem tartalmazza a települést, amit begépelt, vagy esetleg elgépelte azt. Próbáljon meg egy másik helységet megadni!
* Hogyan tudom bezárni programot?
  * A program bezárásához a kis "x"-et kell megadni a város nevénél.

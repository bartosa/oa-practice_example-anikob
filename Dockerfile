# imagename: ubuntu
FROM ubuntu

#RUN apt-get update && apt-get -y install cmake && apt-get -y install g++ && apt-get -y install gcc
# az Y az, hogy mindenre Yes telepites kozben
RUN apt-get update && apt-get -y install cmake g++ gcc qt5-default qt5-qmake

#src mappa tartalmát  a weather/src-be tesz
COPY src/. /weather/src
COPY CMakeLists.txt /weather

RUN mkdir -p /weather/build

WORKDIR /weather/build

RUN cmake ..
RUN make

ENTRYPOINT ["./src/weather"]


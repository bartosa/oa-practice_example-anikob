#include "weather_controller.h"
#include <iostream>
#include <json.hpp>
#include <QtCore>
#include <QUrl>
#include <QVector>

using namespace nlohmann;

WeatherController::WeatherController(QString ip, int port) :
        client(ip.toStdString().c_str(), port)
{
}

QVector<weather> WeatherController::getTemps(QString place)
{
    QVector<weather> temps;
    QString req= "/data/2.5/forecast?q="+place+  //place
                    "&APPID=1339fac94e313fa7bf252709def12be7"+ //My API id
                    "&units=metric"; //Use celsuis, not kelvin
    int status;
    try {
        auto response = client.Get(req.toStdString().c_str(), headers);
        status=response->status;

        json reqBody= json::parse(response->body);

        weather w;
        w.country = reqBody.at("city").at("country"); //gb

        json lists = reqBody.at("list");
        QString time_format = "yyyy-MM-dd HH:mm:ss";
        for (const auto& item : lists.items()){

            string dat=item.value().at("dt_txt");//date
            w.datum=QDateTime::fromString(QString::fromStdString(dat),time_format);
            w.main = (item.value().at("weather"))[0].at("main"); //Rain
            w.temp = (item.value().at("main")).at("temp"); // 15
            w.feel = (item.value().at("main")).at("feels_like"); //16
            w.min = (item.value().at("main")).at("temp_min"); //13
            w.max = (item.value().at("main")).at("temp_max"); //17

            temps.push_back(w);
        }
    }catch (...){
        if(status==404){
            cout<<"Place not found: "<<place.toStdString()<<" Try another!"<<endl;
            return temps;
        }
    }
    return temps;
}




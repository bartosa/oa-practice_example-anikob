#pragma once

#include <httplib.h>
#include <string>
#include <vector>
#include <iostream>
#include <QString>
#include <QNetworkAccessManager>
#include <QObject>
#include <QVector>


using namespace std;
using namespace httplib;

struct weather{
 double temp;
 double feel;
 double min;
 double max;
 string main;
 QDateTime datum;
 string country;
};

 class WeatherController
{
private:
     httplib::Client client;
     httplib::Headers headers;
     QVector<QString> stringVector;

public:
    WeatherController(QString ip, int port);
     QVector<weather>  getTemps(QString place);
};

#include <iostream>
#include "weather_controller.h"

using namespace std;


void printargument(string args, string& datas, bool first, string& header, weather w){
    if (args.length() != 0) { // if there are some arg
        for (int j = 0; j < args.length(); j++) {

            // description
            if (args[j] == 'd' || args[j] == 'D') {
                datas += "   " + QString::fromStdString(w.main).leftJustified(9, ' ').toStdString();
                if (first) { header += "   descr.   "; } // add header
            }

            // feel
            if (args[j] == 'f' || args[j] == 'F') {
                string feeltemp =
                        ((QString("%1 %2")).arg(w.feel, 2, 'f', 2, '0').arg('\xb0')).toStdString() +
                        "C"; // -12.34'C
                datas += " " + QString::fromStdString(feeltemp).rightJustified(9,
                                                                               ' ').toStdString(); // feel
                if (first) { header += "   feel   "; } // add header
            }

            // warning
            if (args[j] == 'w' || args[j] == 'W') {
                if (w.main == "Rain" || w.main == "Snow") {
                    datas += "    " + w.main + "!";// + "!";
                }
                else { datas += "         "; }
                if (first) { header += " warning "; }
            }

            // min max
            if (args[j] == 'm' || args[j] == 'M') {
                string min = ((QString("%1")).arg(w.min, 2, 'f', 2, '0')).toStdString();
                string max =
                        ((QString("%1 %2")).arg(w.max, 2, 'f', 2, '0').arg('\xb0')).toStdString() +
                        "C";
                datas += " " + QString::fromStdString(min).rightJustified(6, ' ').toStdString() +
                         " ~ " +
                         QString::fromStdString(max).rightJustified(9, ' ').toStdString(); // feel
                if (first) { header += " " + QString::fromStdString("min").rightJustified(6, ' ').toStdString() + " ~ max      ";} // add header
            }
        }
    }
}

void getweather(QString city,string args){
    using namespace httplib;

    WeatherController todo_client("api.openweathermap.org", 80);

    QVector<weather> weathers = todo_client.getTemps(city);//(city);
    if (weathers.size() != 0) {
        cout << "\nWeather in " << city.toStdString() << " (" << weathers[0].country << "):"
             << endl; //Weather in London (GB):
        string header = " date  time       tempr ";
        string datas = "";
        int first = true;
        for (auto w : weathers) {
            datas += (w.datum.toString("MM.dd. HH:mm").leftJustified(12, ' ')).toStdString() += ": "; //date
            string temp = ((QString("%1 %2")).arg(w.temp, 2, 'f', 2, '0').arg('\xb0')).toStdString() + "C";
            datas += " " + QString::fromStdString(temp).rightJustified(9, ' ').toStdString(); // temperature

            printargument(args,datas,first,header,w); //arguments

            datas += "\n";
            first = false;
        }
        for (int i = 0; i < header.length(); i++)cout << "-";
        cout << endl << header << endl;
        for (int i = 0; i < header.length(); i++)cout << "-";
        cout << endl;
        cout << datas;
    }
}


int main(int argc, const char* argv[]){

    string cityargs;
    string args="";
    QString city;
    do {
        city="";
        args="";
        cout << "City and arguments (x : exit): ";
        getline(cin, cityargs);

        if(cityargs.find(' ')!=std::string::npos) {//there are arguments
            city = QString::fromStdString(cityargs.substr(0, cityargs.find(' ')));
            args = cityargs.substr(cityargs.find(' '), cityargs.length());
        }
        else {  //no args
            city=QString::fromStdString(cityargs);
        }

        if(city!="x") {
            getweather(city,args);
        }
        // Ez az előrejelzés:
        // api.openweathermap.org/data/2.5/forecast?q=London,uk&APPID=1339fac94e313fa7bf252709def12be7

    }while(city!="x");
}
